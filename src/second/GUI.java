package second;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class GUI {
	private JFrame frame;
	private JRadioButton redButton;
	private JRadioButton greenButton;
	private JRadioButton blueButton;
	private JPanel panel;
	private JPanel background;
	
	public GUI(){
		createFrame();
		//createPanel();
		//createButton();
		
	}
	
	public void createFrame(){
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createPanel(){
		panel = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createRadioButton();
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);
		background.add(panel, BorderLayout.SOUTH);
	}
	
	public void createRadioButton(){
		redButton = new JRadioButton("RED");
		redButton.setSelected(false);
		greenButton = new JRadioButton("GREEN");
		greenButton.setSelected(false);
		blueButton = new JRadioButton("Blue");
		blueButton.setSelected(false);
		redButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(redButton.isSelected()){
					background.setBackground(Color.RED);
				}
				else{
					background.setBackground(null);
				}
				
			}
			
		});
		
		greenButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(greenButton.isSelected()){
					background.setBackground(Color.GREEN);
				}
				else{
					background.setBackground(null);
				}
			}
			
		});
		
		blueButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(blueButton.isSelected()){
					background.setBackground(Color.BLUE);
				}
				else{
					background.setBackground(null);
				}
				
			}
			
		});



	}
}
