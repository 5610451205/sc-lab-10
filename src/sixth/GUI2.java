package sixth;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI2 {
	private static final int FRAME_WIDTH = 600;
	private static final int FRAME_HEIGHT = 300;
	private static final double DEFAULT_RATE = 5;
	private static final double INITIAL_BALANCE = 0;
	private JFrame frame;
	
	private JButton depositButton;
	private JButton withdrawButton;
	private JButton checkButton;
	private JPanel panel;
	private JPanel panel2;
	private JPanel panel3;
	private JPanel background;
	private JTextField inputbalance;
	private JLabel show;
	private BankAccount account;
	
	
	private JLabel rateLabel;
	//private JTextField rateField;
	//private JButton button;
	//private JLabel resultLabel;
	//private JPanel panel;
	//private BankAccount account;
	
	public GUI2(){
		frame = new JFrame();
		background = new JPanel();
		account = new BankAccount(INITIAL_BALANCE);
		panel = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();
		rateLabel = new JLabel("Interest Balance: ");
		inputbalance = new JTextField(20);
		inputbalance.setText("" + DEFAULT_RATE);
		show = new JLabel("balance: " + account.checkBalance());
		
		depositButton = new JButton("Deposit");
		depositButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				double rate = Double.parseDouble(inputbalance.getText());
				account.deposit(rate);
				show.setText("balance: " + account.checkBalance());
			}
			
		});
		
		withdrawButton = new JButton("Withdraw");
		withdrawButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				double rate = Double.parseDouble(inputbalance.getText());
				account.withdraw(rate);
				show.setText("balance: " + account.checkBalance());
				if(account.checkBalance() <= 0){
					
					show.setText("balance: " + INITIAL_BALANCE);
				}
			}
			
		});
		
		checkButton = new JButton("CheckBalance");
		checkButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				show.setText("Check balance: " + account.checkBalance());
			}
			
		});
		
		panel.add(rateLabel);
		panel.add(inputbalance);
		
		panel2.add(depositButton);
		panel2.add(withdrawButton);
		panel2.add(checkButton);
		
		panel3.add(show);
		
		background.setLayout(new BorderLayout());
		background.add(panel2, BorderLayout.EAST);
		background.add(panel, BorderLayout.CENTER);
		background.add(panel3, BorderLayout.SOUTH);
		
		
		frame.add(background);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
/*
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		account = new BankAccount(INITIAL_BALANCE);
		// Use instance variables for components
		resultLabel = new JLabel("balance: " + account.checkBalance());
		// Use helper methods
		createTextField();
		createButton();
		createPanel();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		} 

	private void createTextField(){
		rateLabel = new JLabel("Interest Rate: ");
		final int FIELD_WIDTH = 10;
		rateField = new JTextField(FIELD_WIDTH);
		rateField.setText("" + DEFAULT_RATE);
		}

	private void createButton(){
		button = new JButton("Add Interest");
		class AddInterestListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				double rate = Double.parseDouble(rateField.getText());
				double interest = account.checkBalance() * rate / 100;
				account.deposit(interest);
				resultLabel.setText("balance: " + account.checkBalance());
			}
			}
		}
	 

	private void createPanel()
	{
		panel = new JPanel();
		panel.add(rateLabel);
		panel.add(rateField);
		panel.add(button);
		panel.add(resultLabel);
		frame.add(panel);
	}*/	
	}

}
