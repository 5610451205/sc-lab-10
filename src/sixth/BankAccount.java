package sixth;

public class BankAccount {
	private double balance;
	public BankAccount(double initialBalance){
		this.balance = initialBalance;
	}
	
	public void deposit(double interest){
		this.balance = this.balance + interest;
	}
	
	public void withdraw(double balance){
		this.balance = this.balance - balance;
	}
	
	public double checkBalance(){
		return this.balance;
	}
	
}
